package com.example.shoppapplication.util

sealed class Resource<T>(val data:T? = null,val errorMessage:String? = null){
    class Success<T>(data: T):Resource<T>(data)
    class Error<T>(errorMessage: String,data: T? = null):Resource<T>(data,errorMessage)
    class Loading<T>():Resource<T>()
}



//data class Resource<T>(
//    val status: Status,
//    val data: T? = null,
//    val message: String? = null
//) {
//    companion object {
//        fun <T> success(data: T): Resource<T> {
//            return Resource(Status.SUCCESS, data)
//        }
//
//        fun <T> error(message: String?): Resource<T> {
//            return Resource(Status.ERROR, null, message)
//        }
//
//        fun <T> loading(): Resource<T> {
//            return Resource(Status.LOADING,)
//        }
//    }
//}
//
//enum class Status {
//    SUCCESS, ERROR, LOADING
//}
