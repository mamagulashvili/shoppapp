package com.example.shoppapplication.util

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthPreference @Inject constructor(@ApplicationContext context: Context) {
    companion object {
        const val HAS_CHECKED = "HAS_CHECKED"
        const val TOKEN = "TOKEN"
        const val USER_ID = "USER_ID"
    }

    private val sharedPreferences: SharedPreferences by lazy {
        context.getSharedPreferences("userPreference", Context.MODE_PRIVATE)
    }

    fun saveAuthState(checked: Boolean) {
        sharedPreferences.edit().putBoolean(HAS_CHECKED, checked).apply()
    }

    fun saveToken(token: String) {
        sharedPreferences.edit().putString(TOKEN, token).apply()
    }

    fun saveUserId(userId: Int) {
        sharedPreferences.edit().putInt(USER_ID, userId).apply()
    }

    fun getUserId() = sharedPreferences.getInt(USER_ID, 0)
    fun getToken() = sharedPreferences.getString(TOKEN, "")
    fun getAuthState() = sharedPreferences.getBoolean(HAS_CHECKED, false)
}