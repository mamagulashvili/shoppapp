package com.example.shoppapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppapplication.databinding.RowDrawerItemBinding
import com.example.shoppapplication.model.drawer.DrawerItem
typealias onMenuClick= (id:Int) -> Unit
class DrawerAdapter : RecyclerView.Adapter<DrawerAdapter.DrawerViewHolder>() {

    private val itemList = mutableListOf<DrawerItem>()
    lateinit var onMenuClick: onMenuClick


    inner class DrawerViewHolder(val binding: RowDrawerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: DrawerItem
        fun onBind() {
            model = itemList[adapterPosition]
            binding.apply {
                tvItem.text = model.itemName
                root.setOnClickListener{
                    onMenuClick.invoke(adapterPosition)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawerViewHolder =
        DrawerViewHolder(
            RowDrawerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: DrawerViewHolder, position: Int) = holder.onBind()

    override fun getItemCount(): Int = itemList.size

    fun setData(newList: MutableList<DrawerItem>) {
        this.itemList.apply {
            clear()
            addAll(newList)
        }
        notifyDataSetChanged()
    }

}