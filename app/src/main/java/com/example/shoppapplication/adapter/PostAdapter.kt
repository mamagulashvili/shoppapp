package com.example.shoppapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.example.shoppapplication.databinding.RowPostItemBinding
import com.example.shoppapplication.model.post.Post
import com.example.shoppapplication.ui.fragments.auth.login.string


class PostAdapter : RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    companion object {
        private const val USD_PRICE_TYPE = "1"
        private const val GEL_PRICE_TYPE = "GEL"
    }

    inner class PostViewHolder(val binding: RowPostItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var model: Post

        fun onBind() {
            model = differ.currentList[adapterPosition]
            binding.apply {
                tvPostName.text = model.title
                tvDescription.text = model.description

                when (model.priceType) {
                    USD_PRICE_TYPE -> {
                        tvPrice.text =
                            root.context.resources.getString(string.price, "$", model.price.toInt())
                    }
                    GEL_PRICE_TYPE -> tvPrice.text =
                        root.context.resources.getString(string.price, "₾", model.price.toInt())
                }
                setViewPager()

            }
        }

        private fun setViewPager() {
            val imageList = mutableListOf<String>()
            model.urls.forEach {
                imageList.add(it.url)
            }
            when (imageList.size) {
                0, 1 -> {
                    binding.apply {
                        ivLeftArrow.isVisible = false
                        ivRightArrow.isVisible = false
                    }
                }
                else -> {
                    binding.apply {
                        ivRightArrow.setOnClickListener {
                            vpPostImage.setCurrentItem(vpPostImage.currentItem + 1, true)
                        }
                        ivLeftArrow.setOnClickListener {
                            vpPostImage.setCurrentItem(vpPostImage.currentItem - 1, true)
                        }
                    }
                }
            }
            binding.apply {
                tvImageQuantity.text =
                    root.context.resources.getString(string.custom_text, "/", imageList.size)
                val vpAdapter = ViewPagerAdapter(imageList)
                vpPostImage.adapter = vpAdapter

                tvCurrentImageIndex.text = "${vpPostImage.currentItem}"
            }

            binding.vpPostImage.registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    setCurrentImageIndex(position + 1)
                }
            })
        }

        private fun setCurrentImageIndex(index: Int) {
            binding.tvCurrentImageIndex.text = index.toString()

        }
    }

    private val diffUtil = object : DiffUtil.ItemCallback<Post>() {
        override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
            return oldItem == newItem
        }
    }
    val differ = AsyncListDiffer(this, diffUtil)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder =
        PostViewHolder(
            RowPostItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) = holder.onBind()

    override fun getItemCount(): Int = differ.currentList.size

}