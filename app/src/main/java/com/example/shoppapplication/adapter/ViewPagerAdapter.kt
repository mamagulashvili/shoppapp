package com.example.shoppapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppapplication.databinding.RowViewPagerItemBinding
import com.example.shoppapplication.extensions.getImage

class ViewPagerAdapter(private val imageList: MutableList<String>) :
    RecyclerView.Adapter<ViewPagerAdapter.ViewPagerViewHolder>() {

    inner class ViewPagerViewHolder(val binding: RowViewPagerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.root.getImage(imageList[adapterPosition])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPagerViewHolder =
        ViewPagerViewHolder(
            RowViewPagerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: ViewPagerViewHolder, position: Int) = holder.onBind()

    override fun getItemCount(): Int = imageList.size
}