package com.example.shoppapplication.network

import com.example.shoppapplication.model.LogInResponse
import com.example.shoppapplication.model.SignUpResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthService {
    @POST("login")
    @FormUrlEncoded
    suspend fun logIn(
        @Field("email") email: String,
        @Field("password") password: String
    ): Response<LogInResponse>

    @POST("register")
    @FormUrlEncoded
    suspend fun signUp(
        @Field("email")
        email: String,
        @Field("password")
        password: String,
        @Field("full_name")
        fullName:String
    ): Response<SignUpResponse>
}