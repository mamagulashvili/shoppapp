package com.example.shoppapplication.network

import com.example.shoppapplication.model.CompleteProfile
import com.example.shoppapplication.model.CompleteProfileStatus
import com.example.shoppapplication.model.Profile
import com.example.shoppapplication.model.post.Post
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface PostService {

    @GET("posts")
    suspend fun getPosts(): Response<List<Post>>

    @POST("profile")
    @FormUrlEncoded
    suspend fun completeProfile(@Field("user_id") userID: Int): Response<CompleteProfileStatus>

    @POST("complete-profile")
    @Multipart
    suspend fun completeProfile(
        @Part("user_id") userId: Int,
        @Part("address") address: String,
        @Part("card_number") cardNumber: String,
        @Part("expiry_date") expiryDate: Int,
        @Part("security_code") securityCode: Int,
        @Part("floor_apartment") floorApartment: Int,
        @Part("file") file: MultipartBody.Part,
    ): Response<CompleteProfile>
}