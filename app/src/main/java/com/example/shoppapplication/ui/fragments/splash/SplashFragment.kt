package com.example.shoppapplication.ui.fragments.splash

import android.animation.Animator
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.shoppapplication.R
import com.example.shoppapplication.databinding.SplashFragmentBinding
import com.example.shoppapplication.extensions.hide
import com.example.shoppapplication.ui.base.BaseFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment : BaseFragment<SplashFragmentBinding>(SplashFragmentBinding::inflate) {

    private val viewModel: SplashViewModel by viewModels()

    override fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) {
        init()
    }

    private fun init() {
        binding.lottieAnim.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {}

            override fun onAnimationEnd(animation: Animator?) {
                navigateToNextFragment()
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationRepeat(animation: Animator?) {

            }

        })
    }

    private fun navigateToNextFragment() {
        if (viewModel.isAuthorized()) {
            findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
        } else {
            findNavController().navigate(R.id.action_splashFragment_to_signInFragment)
        }
    }
}