package com.example.shoppapplication.ui.fragments.auth.login

import android.util.Log.d
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shoppapplication.model.LogInResponse
import com.example.shoppapplication.repositories.auth.AuthRepositoryImplementation
import com.example.shoppapplication.repositories.post.PostRepositoryImpl
import com.example.shoppapplication.util.AuthPreference
import com.example.shoppapplication.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val repo: AuthRepositoryImplementation,
    private val authPreference: AuthPreference,
    private val postRepositoryImpl: PostRepositoryImpl
) : ViewModel() {

    private val _logIn by lazy {
        MutableLiveData<Resource<LogInResponse>>()
    }
    val logIn: LiveData<Resource<LogInResponse>> = _logIn


    fun logIn(email: String, password: String, rememberMe: Boolean) = viewModelScope.launch {
        _logIn.postValue(Resource.Loading())
        withContext(Dispatchers.IO) {
            val response = repo.logIn(email, password, rememberMe)
            _logIn.postValue(response)
        }
    }

    private val _profileStatus by lazy {
        MutableLiveData<Resource<Boolean>>()
    }
    val profileStatus: LiveData<Resource<Boolean>> = _profileStatus

    fun getProfileStatus() = viewModelScope.launch {
        _profileStatus.postValue(Resource.Loading())
        withContext(Dispatchers.IO) {
            val userId = authPreference.getUserId()
            d("RESPONSEV","${userId}")
            d("RESPONSEV","${postRepositoryImpl.getProfileStatus(userId)}")
            _profileStatus.postValue(postRepositoryImpl.getProfileStatus(userId))
        }
    }
}