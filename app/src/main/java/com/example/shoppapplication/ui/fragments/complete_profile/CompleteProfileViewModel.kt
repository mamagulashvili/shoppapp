package com.example.shoppapplication.ui.fragments.complete_profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shoppapplication.repositories.post.PostRepositoryImpl
import com.example.shoppapplication.util.AuthPreference
import com.example.shoppapplication.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class CompleteProfileViewModel @Inject constructor(
    private val repo: PostRepositoryImpl,
    private val authPreference: AuthPreference
) : ViewModel() {

}