package com.example.shoppapplication.ui.fragments.home

import android.os.Build
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shoppapplication.R
import com.example.shoppapplication.adapter.DrawerAdapter
import com.example.shoppapplication.databinding.HomeFragmentBinding
import com.example.shoppapplication.model.drawer.DrawerItem
import com.example.shoppapplication.ui.base.BaseFragment
import com.example.shoppapplication.ui.fragments.auth.login.string
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeFragmentBinding>(HomeFragmentBinding::inflate) {

    private lateinit var navController: NavController
    private lateinit var toggle: ActionBarDrawerToggle
    private val drawerAdapter by lazy { DrawerAdapter() }

    override fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) {
        init()
    }

    private fun init() {
        initViews()
        initDrawerRecycle()
    }


    private fun initViews() {
        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.bottomNavHostFragment) as NavHostFragment
        navController = navHostFragment.findNavController()
        binding.bottomNavView.setupWithNavController(navController)

        (requireActivity() as AppCompatActivity).apply {
            setSupportActionBar(binding.toolbar)
            supportActionBar?.title = null
        }
        binding.btnOpenDrawer.setOnClickListener {
            binding.root.openDrawer(GravityCompat.END)
        }
        
        setHasOptionsMenu(true)
    }

    private fun initDrawerRecycle() {
        val navHostFragment =
            requireActivity().supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        navController = navHostFragment.findNavController()
        binding.rvDrawer.apply {
            clipToPadding = false
            layoutManager = LinearLayoutManager(requireContext())
            adapter = drawerAdapter
        }
        drawerAdapter.setData(
            mutableListOf(
                DrawerItem(getString(string.my_profile)),
                DrawerItem(getString(string.my_posts)),
                DrawerItem(getString(string.about)),
                DrawerItem(getString(string.my_cart)),
            )
        )
        drawerAdapter.onMenuClick = {
            navController.navigate(R.id.action_global_signInFragment)
        }
    }
}