package com.example.shoppapplication.ui.fragments.auth.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shoppapplication.model.SignUpResponse
import com.example.shoppapplication.repositories.auth.AuthRepositoryImplementation
import com.example.shoppapplication.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val repo: AuthRepositoryImplementation
) : ViewModel() {
    private val _signUp by lazy {
        MutableLiveData<Resource<SignUpResponse>>()
    }
    val signUp: LiveData<Resource<SignUpResponse>> = _signUp

    fun signUp(email: String, password: String, fullName: String) = viewModelScope.launch {
        _signUp.postValue(Resource.Loading())
        withContext(Dispatchers.IO) {
            val response = repo.signUp(email, password, fullName)
            _signUp.postValue(response)
        }
    }
}