package com.example.shoppapplication.ui.fragments.wallet

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.shoppapplication.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyWalletFragment : Fragment() {

}