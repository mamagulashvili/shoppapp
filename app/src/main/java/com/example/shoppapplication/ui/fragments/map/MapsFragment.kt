package com.example.shoppapplication.ui.fragments.map

import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.shoppapplication.R
import com.example.shoppapplication.databinding.FragmentMapsBinding
import com.example.shoppapplication.ui.base.BaseFragment

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsFragment : BaseFragment<FragmentMapsBinding>(FragmentMapsBinding::inflate) {

    private val callback = OnMapReadyCallback { googleMap ->
        val dvani = LatLng(43.8756904, 42.1574472)
        googleMap.addMarker(MarkerOptions().position(dvani).title("this iz dvane"))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(dvani))
    }

    override fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) {
        init()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }
    private fun init(){

    }

}