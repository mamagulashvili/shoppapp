package com.example.shoppapplication.ui.fragments.complete_profile

import android.Manifest
import android.content.ContentValues
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.fragment.app.viewModels
import com.example.shoppapplication.R
import com.example.shoppapplication.databinding.CompleteProfileFragmentBinding
import com.example.shoppapplication.ui.base.BaseFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CompleteProfileFragment :
    BaseFragment<CompleteProfileFragmentBinding>(CompleteProfileFragmentBinding::inflate) {

    private val viewModel: CompleteProfileViewModel by viewModels()
    private var uri: Uri? = null
    private var googleMap:GoogleMap? = null
    private val callback = OnMapReadyCallback { googleMap ->
        this.googleMap = googleMap
        val dvani = LatLng(43.8756904, 42.1574472)
        googleMap.addMarker(MarkerOptions().position(dvani).title("this iz dvane"))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(dvani))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    override fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) {
        init()
    }

    private fun init() {
        binding.ivProfileImage.setOnClickListener {
            checkPermissions()
        }
    }

    private fun checkPermissions() {
        when {
            hasCameraPermission() && hasReadExternalStoragePermission() && hasWriteExternalStoragePermission() -> {
                openCamera()
            }
            ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.CAMERA
            ) -> {
                Snackbar.make(
                    requireView(),
                    "App needs This permission",
                    Snackbar.LENGTH_INDEFINITE
                ).apply {
                    setAction("OK") {
                        requestPermissions(permissionLauncher)
                    }
                }
            }
            else -> requestPermissions(permissionLauncher)
        }

    }

    private val permissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
        }

    private fun openCamera() {
        val filename = "profile.jpg"
        val imageUri =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
            } else {
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            }
        val imageDetail = ContentValues().apply {
            put(MediaStore.Audio.Media.DISPLAY_NAME, filename)
        }
        requireActivity().contentResolver.insert(imageUri, imageDetail).let {
            uri = it
            takePicture.launch(uri)
        }

    }

    private val takePicture = registerForActivityResult(ActivityResultContracts.TakePicture()) {
        binding.ivProfileImage.setImageURI(uri)
    }
}