package com.example.shoppapplication.ui.fragments.auth.signup

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.shoppapplication.R
import com.example.shoppapplication.databinding.SignUpFragmentBinding
import com.example.shoppapplication.extensions.*
import com.example.shoppapplication.ui.base.BaseFragment
import com.example.shoppapplication.ui.fragments.auth.login.colors
import com.example.shoppapplication.ui.fragments.auth.login.drawable
import com.example.shoppapplication.ui.fragments.auth.login.string
import com.example.shoppapplication.util.Resource
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpFragment : BaseFragment<SignUpFragmentBinding>(SignUpFragmentBinding::inflate) {

    private val viewModel: SignUpViewModel by viewModels()

    override fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) {
        init()
    }

    private fun init() {

        setListeners()
        setSpannedText()
        validInputs()
        observe()
    }

    private fun setListeners() {
        binding.apply {
            tvAlreadyHaveAcc.setOnClickListener {
                findNavController().navigate(R.id.action_signUpFragment_to_signInFragment)
            }
            btnSignUp.setOnClickListener {
                signUp()
            }
        }
    }

    private fun signUp() {
        val email = binding.etEmailAddress.text.toString()
        val password = binding.etPassword.text.toString()
        val repeatPassword = binding.etRepeatPassword.text.toString()
        val fullName = binding.etFullName.text.toString()
        if (email.isNotBlank() && password.isNotBlank() && repeatPassword.isNotBlank() && fullName.isNotBlank()) {
            if (password == repeatPassword) {
                viewModel.signUp(email, password, fullName)
            } else {
                binding.root.createSnack(getString(string.please_repeat_password), Color.RED)
            }
        } else {
            binding.root.createSnack(getString(string.please_fill_all_field), Color.RED)
        }
    }

    private fun observe() {
        viewModel.signUp.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Success -> {
                    binding.progressBar.hide()
                    findNavController().navigate(R.id.action_signUpFragment_to_signInFragment)
                }
                is Resource.Error -> {
                    binding.progressBar.hide()
                    it.errorMessage?.let { it1 -> showErrorDialog(it1) }
                }
                is Resource.Loading -> {
                    binding.progressBar.show()
                }
            }
        })
    }

    private fun validInputs() {
        binding.etEmailAddress.doOnTextChanged { text, _, _, _ ->
            if (text?.isValidEmail()!!) {
                binding.tilEmailAddress.apply {
                    error = null
                    setEndIconDrawable(drawable.ic_check)
                    endIconMode = TextInputLayout.END_ICON_CUSTOM
                }
            } else {
                binding.tilEmailAddress.error = getString(string.incorrect_email)
            }
        }
    }

    private fun setSpannedText() {
        binding.tvAlreadyHaveAcc.setMultipleTextColor(
            mutableListOf(
                getString(string.already_have_a_account),
                getString(string.login)
            ), mutableListOf(
                colors.gray,
                colors.main_color
            )
        )
    }
}