package com.example.shoppapplication.ui.fragments.post

import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shoppapplication.R
import com.example.shoppapplication.adapter.PostAdapter
import com.example.shoppapplication.databinding.PostFragmentBinding
import com.example.shoppapplication.ui.base.BaseFragment
import com.example.shoppapplication.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PostFragment : BaseFragment<PostFragmentBinding>(PostFragmentBinding::inflate) {

    private val viewModel: PostViewModel by viewModels()
    private val postAdapter: PostAdapter by lazy { PostAdapter() }

    override fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) {
        init()
    }

    private fun init() {
        lifecycleScope.launch {
            viewModel.getPost()
        }
        observePosts()
        initRv()
        binding.root.setOnRefreshListener {
            viewModel.getPost()
            postAdapter.notifyDataSetChanged()
        }
        binding.fabAddPost.setOnClickListener {
            findNavController().navigate(R.id.action_FragmentHome_to_addPostFragment)
        }
    }

    private fun observePosts() {
        viewModel.posts.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Success -> {
                    binding.root.isRefreshing = false
                    binding.tvError.isVisible = false
                    postAdapter.differ.submitList(it.data)
                    d("POSTRESPONSE", "${it.data}")
                }
                is Resource.Error -> {
                    binding.root.isRefreshing = false
                    binding.tvError.isVisible = true
                    d("POSTRESPONSE", "${it.errorMessage}")
                }
                is Resource.Loading -> {
                    binding.tvError.isVisible = false
                    binding.root.isRefreshing = true
                }
            }
        })
    }


    private fun initRv() {
        binding.rvPosts.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = postAdapter
        }
    }
}