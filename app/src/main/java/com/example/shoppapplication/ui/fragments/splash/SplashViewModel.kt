package com.example.shoppapplication.ui.fragments.splash

import androidx.lifecycle.ViewModel
import com.example.shoppapplication.util.AuthPreference
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val authPreference: AuthPreference
) : ViewModel() {

    fun isAuthorized() = authPreference.getAuthState()
}