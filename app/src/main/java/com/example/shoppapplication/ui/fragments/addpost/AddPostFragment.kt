package com.example.shoppapplication.ui.fragments.addpost

import android.content.pm.PackageManager
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.example.shoppapplication.R
import com.example.shoppapplication.databinding.AddPostFragmentBinding
import com.example.shoppapplication.ui.base.BaseFragment


class AddPostFragment : BaseFragment<AddPostFragmentBinding>(AddPostFragmentBinding::inflate) {

    override fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) {
        init()
    }

    private fun init() {
        setArrayAdapters()
        requestPermissions(permissionLauncher)
        binding.cvImage.setOnClickListener {
            permissionsChecker()

        }
    }

    private val imageContract =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            uri?.let {
                binding.cameraIc.isVisible = false
                binding.ivPostImage.setImageURI(it)
            }
        }

    private val permissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            permissions.entries.forEach {
                val isGranted = it.value
                if (isGranted) {
                    Toast.makeText(requireContext(), "grated", Toast.LENGTH_SHORT).show()
                } else {
                    permissionsChecker()
                }
            }

        }

    private fun permissionsChecker() {
        when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.CAMERA,
            ) -> {
                addImage()
            }
            ContextCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) -> {

            }
            ContextCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) -> {

            }
            else -> requestPermissions(permissionLauncher)
        }
    }

    private fun openCamera() {

    }

    private fun addImage() {
        imageContract.launch("image/*")
    }

    private fun setArrayAdapters() {
        val categories = resources.getStringArray(R.array.category)
        val categoryAdapter =
            ArrayAdapter(requireContext(), R.layout.dropdown_menu_item, categories)
        val priceTypes = resources.getStringArray(R.array.price_type)
        val typesAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_menu_item, priceTypes)
        binding.apply {
            atcCategory.setAdapter(categoryAdapter)
            actPrice.setAdapter(typesAdapter)
        }

    }
}