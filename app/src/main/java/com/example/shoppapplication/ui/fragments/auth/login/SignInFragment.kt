package com.example.shoppapplication.ui.fragments.auth.login

import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.shoppapplication.R
import com.example.shoppapplication.databinding.SignInFragmentBinding
import com.example.shoppapplication.extensions.*
import com.example.shoppapplication.ui.base.BaseFragment
import com.example.shoppapplication.util.Resource
import dagger.hilt.android.AndroidEntryPoint

typealias string = R.string
typealias colors = R.color
typealias drawable = R.drawable

@AndroidEntryPoint
class SignInFragment : BaseFragment<SignInFragmentBinding>(SignInFragmentBinding::inflate) {

    private val viewModel: SignInViewModel by viewModels()

    override fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) {
        init()
    }

    private fun init() {


        setSpannedText()
        setListeners()
        validInputs()

        binding.tilEmailAddress.isEndIconVisible = false
    }

    private fun setListeners() {
        binding.apply {
            btnSignIn.setOnClickListener {
                viewModel.getProfileStatus()
                logIn()

            }
            tvSignUp.setOnClickListener {
                findNavController().navigate(R.id.action_signInFragment_to_signUpFragment)
            }
        }
    }

    private fun observeProfileStatus() {
        viewModel.profileStatus.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Success -> {
                    d("RESPONSE","${it.data}")
                    binding.progressBar.hide()
                    if (it.data!!) {
                        findNavController().navigate(R.id.action_signInFragment_to_homeFragment)
                    } else {
                        findNavController().navigate(R.id.action_signInFragment_to_completeProfileFragment)
                    }
                }
                is Resource.Error -> { binding.progressBar.hide()}
                is Resource.Loading -> {binding.progressBar.show()}
            }
        })
    }

    private fun logIn() {
        val email = binding.etEmailAddress.text.toString()
        val password = binding.etPassword.text.toString()
        if (email.isNotBlank() && password.isNotBlank()) {
            if (email.isValidEmail()) {
                viewModel.logIn(email, password, binding.checkboxRememberMe.isChecked)
            } else {
                binding.root.createSnack(getString(string.please_enter_valid_email))
            }
        } else {
            binding.root.createSnack(getString(string.please_fill_all_field))
        }
        observeLogInResponse()
        observeProfileStatus()
    }

    private fun observeLogInResponse() {
        viewModel.logIn.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Success -> {
                    d("RESPONSEL","${it.data}")
//                    findNavController().navigate(R.id.action_signInFragment_to_homeFragment)
                    binding.progressBar.hide()
                }
                is Resource.Error -> {
                    binding.progressBar.hide()
                    it.errorMessage?.let { message -> showErrorDialog(message) }
                }
                is Resource.Loading -> {
                    binding.progressBar.show()
                }
            }
        })
    }

    private fun validInputs() {
        binding.etEmailAddress.doOnTextChanged { text, _, _, _ ->
            if (text?.isValidEmail()!!) {
                binding.tilEmailAddress.apply {
                    error = null
                    binding.tilEmailAddress.isEndIconVisible = true
                }
            } else {
                binding.tilEmailAddress.isEndIconVisible = false
                binding.tilEmailAddress.error = getString(string.incorrect_email)
            }
        }
    }

    private fun setSpannedText() {
        binding.tvSignUp.setMultipleTextColor(
            mutableListOf(
                getString(string.new_user),
                getString(string.sign_up),
                getString(string.here)
            ),
            mutableListOf(
                colors.gray,
                colors.main_color,
                colors.gray
            )
        )
    }

}