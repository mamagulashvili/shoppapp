package com.example.shoppapplication.ui.base

import android.Manifest
import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.example.shoppapplication.databinding.DialogErrorItemBinding
import com.example.shoppapplication.extensions.setDialog

typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VB : ViewBinding>(private val inflate: Inflate<VB>) : Fragment() {

    private var _binding: VB? = null
    protected val binding: VB get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(layoutInflater, container, false)
        start(layoutInflater, container)
        return binding.root
    }

    abstract fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?)

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    protected fun hasCameraPermission() = ActivityCompat.checkSelfPermission(
        requireContext(),
        Manifest.permission.CAMERA
    ) == PackageManager.PERMISSION_GRANTED

    protected fun hasWriteExternalStoragePermission() = ActivityCompat.checkSelfPermission(
        requireContext(),
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED

    protected fun hasReadExternalStoragePermission() = ActivityCompat.checkSelfPermission(
        requireContext(),
        Manifest.permission.READ_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED

    protected fun requestPermissions(permRequest: ActivityResultLauncher<Array<String>>) {
        permRequest.launch(
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
        )
    }


    protected fun showErrorDialog(message: String) {
        val dialog = Dialog(requireContext())
        val dialogBinding = DialogErrorItemBinding.inflate(layoutInflater)
        dialog.setDialog(dialogBinding)
        dialogBinding.tvError.text = message
        dialogBinding.btnRetry.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
}