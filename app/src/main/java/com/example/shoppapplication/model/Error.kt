package com.example.shoppapplication.model

data class Error(
    val ok: Boolean, val errorMessage: String
)