package com.example.shoppapplication.model

import com.google.gson.annotations.SerializedName

data class LogInResponse(
    @SerializedName("user_id")
    val userId: Int?,
    val token: String?
)
