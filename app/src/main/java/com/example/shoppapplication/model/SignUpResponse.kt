package com.example.shoppapplication.model

import com.google.gson.annotations.SerializedName

data class SignUpResponse(
    @SerializedName("OK")
    val ok:Boolean?,
    val registered:Boolean?
)
