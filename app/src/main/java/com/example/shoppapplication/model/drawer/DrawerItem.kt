package com.example.shoppapplication.model.drawer

data class DrawerItem(
    val itemName:String
)
