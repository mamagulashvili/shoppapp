package com.example.shoppapplication.model

import com.google.gson.annotations.SerializedName

data class Profile(
    val address: String,
    @SerializedName("card_holder_name")
    val cardHolderName: String,
    @SerializedName("card_number")
    val cardNumber: String?,
    val email: String?,
    @SerializedName("expiry_date")
    val expiryDate: String?,
    @SerializedName("floor_apartment")
    val floorApartment: String?,
    @SerializedName("full_name")
    val fullName: String?,
    @SerializedName("profile-completed")
    val profileCompleted: Boolean?,
    @SerializedName("profile_url")
    val profileUrl: String?,
    @SerializedName("security_code")
    val securityCode: String?
)
