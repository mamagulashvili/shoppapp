package com.example.shoppapplication.model

import com.google.gson.annotations.SerializedName

data class CompleteProfile(
    val OK: Boolean,
    @SerializedName("profile complete")
    val profileCompleted: Boolean
)