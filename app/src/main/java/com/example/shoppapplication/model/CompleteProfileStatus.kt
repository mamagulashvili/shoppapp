package com.example.shoppapplication.model

import com.google.gson.annotations.SerializedName

data class CompleteProfileStatus(
    val email: String,
    @SerializedName("full_name")
    val fullName: String,
    @SerializedName("profile-completed")
    val profileCompleted: Boolean
)