package com.example.shoppapplication.di

import com.example.shoppapplication.BuildConfig
import com.example.shoppapplication.network.AuthService
import com.example.shoppapplication.network.PostService
import com.example.shoppapplication.repositories.auth.AuthRepository
import com.example.shoppapplication.repositories.auth.AuthRepositoryImplementation
import com.example.shoppapplication.repositories.post.PostRepository
import com.example.shoppapplication.repositories.post.PostRepositoryImpl
import com.example.shoppapplication.util.AuthPreference
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    private const val BASE_URL = "https://ktorhighsteaks.herokuapp.com"

    private fun httpClient(): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
            .addInterceptor(Interceptor { chain ->
                chain.request().url
                val request = chain.request().newBuilder()
                val response = chain.proceed(request.build())
                response
            })
        if (BuildConfig.BUILD_TYPE == "debug") {
            clientBuilder.addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            )
        }
        return clientBuilder.build()
    }

    @Provides
    @Singleton
    fun provideAuthApi(): AuthService = Retrofit.Builder()
        .baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
        .client(httpClient())
        .build()
        .create(AuthService::class.java)

    @Provides
    @Singleton
    fun provideRepository(
        authService: AuthService,
        authPreference: AuthPreference
    ): AuthRepository =
        AuthRepositoryImplementation(authService, authPreference)

    @Provides
    @Singleton
    fun providePostService(): PostService = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(httpClient())
        .build()
        .create(PostService::class.java)

    @Provides
    @Singleton
    fun providePostRepository(postService: PostService): PostRepository =
        PostRepositoryImpl(postService)
}