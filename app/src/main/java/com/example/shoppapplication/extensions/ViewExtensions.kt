package com.example.shoppapplication.extensions

import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar

fun ProgressBar.show() {
    this.isVisible = true
}

fun ProgressBar.hide() {
    this.isVisible = false
}

fun View.createSnack(message: String, color: Int? = null, length: Int = Snackbar.LENGTH_SHORT) {
    val snackBar = Snackbar.make(this, message, length)
    snackBar.apply {
        setTextColor(color!!)
        show()
    }
}
