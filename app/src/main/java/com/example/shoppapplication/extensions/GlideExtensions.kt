package com.example.shoppapplication.extensions

import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.example.shoppapplication.R

fun AppCompatImageView.getImage(imageUrl: String) {
    Glide.with(context).load(imageUrl).into(this)
}