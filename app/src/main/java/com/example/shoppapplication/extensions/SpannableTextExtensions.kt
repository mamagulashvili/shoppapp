package com.example.shoppapplication.extensions

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat

fun AppCompatTextView.setMultipleTextColor(text: MutableList<String>, colors: MutableList<Int>) {
    val spannableText = SpannableString(text.joinToString(""))
    var startIndex = 0
    for (i in text.indices) {
        spannableText.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, colors[i])),
            startIndex, text[i].length + startIndex,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        startIndex += text[i].length
    }
    this.text = spannableText
}