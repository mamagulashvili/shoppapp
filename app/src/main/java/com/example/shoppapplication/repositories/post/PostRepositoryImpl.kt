package com.example.shoppapplication.repositories.post

import android.util.Log.d
import com.example.shoppapplication.model.post.Post
import com.example.shoppapplication.network.PostService
import com.example.shoppapplication.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PostRepositoryImpl @Inject constructor(
    private val postService: PostService,
) : PostRepository {
    override suspend fun getPosts(): Resource<List<Post>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = postService.getPosts()
            if (response.isSuccessful) {
                val result = response.body()!!
                Resource.Success(result)
            } else {
                Resource.Error(response.errorBody().toString())
            }
        } catch (e: Exception) {
            Resource.Error(e.toString())
        }
    }

    override suspend fun getProfileStatus(userId: Int): Resource<Boolean> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val response = postService.completeProfile(userId)
                d("RESPONSER", "${response}")
                Resource.Success(false)

//                if (response.isSuccessful) {
//                    val result = response.body()!!
//                    Resource.Success(result.profileCompleted!!)
//                } else {
//
//                    Resource.Error(response.errorBody().toString())
//                }
            } catch (e: Exception) {
                d("RESPONSER", "${e.toString()}")
                Resource.Error(e.toString())
            }
        }
}