package com.example.shoppapplication.repositories.post

import com.example.shoppapplication.model.post.Post
import com.example.shoppapplication.util.Resource

interface PostRepository {

    suspend fun getPosts(): Resource<List<Post>>
    suspend fun getProfileStatus(userId: Int): Resource<Boolean>
}