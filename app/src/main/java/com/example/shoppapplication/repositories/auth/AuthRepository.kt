package com.example.shoppapplication.repositories.auth

import com.example.shoppapplication.model.LogInResponse
import com.example.shoppapplication.model.SignUpResponse
import com.example.shoppapplication.util.Resource

interface AuthRepository {

    suspend fun logIn(email:String,password:String,rememberMe:Boolean):Resource<LogInResponse>
    suspend fun signUp(email: String,password: String,fullName:String):Resource<SignUpResponse>
}