package com.example.shoppapplication.repositories.auth

import com.example.shoppapplication.model.Error
import com.example.shoppapplication.model.LogInResponse
import com.example.shoppapplication.model.SignUpResponse
import com.example.shoppapplication.network.AuthService
import com.example.shoppapplication.util.AuthPreference
import com.example.shoppapplication.util.Resource
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AuthRepositoryImplementation @Inject constructor(
    private val authApiService: AuthService,
    private val authPreference: AuthPreference
) : AuthRepository {
    override suspend fun logIn(
        email: String,
        password: String,
        rememberMe: Boolean
    ): Resource<LogInResponse> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = authApiService.logIn(email, password)
            if (response.isSuccessful) {
                val responseBody = response.body()!!
                authPreference.apply {
                    saveAuthState(rememberMe)
                    responseBody.token?.let { saveToken(it) }
                    responseBody.userId?.let { saveUserId(it) }
                }
                Resource.Success(responseBody)
            } else {
                val errorBody = Gson().fromJson(response.errorBody()!!.string(), Error::class.java)
                Resource.Error(errorBody.errorMessage)
            }
        } catch (e: Exception) {
            return@withContext Resource.Error(e.toString())
        }
    }

    override suspend fun signUp(
        email: String,
        password: String,
        fullName: String
    ): Resource<SignUpResponse> = withContext(Dispatchers.IO) {
        try {
            val response = authApiService.signUp(email, password, fullName)
            if (response.isSuccessful) {
                Resource.Success(response.body()!!)
            } else {
                val errorBody =
                    Gson().fromJson(response.errorBody()!!.string(), Error::class.java)
                Resource.Error(errorBody.errorMessage)
            }
        } catch (e: Exception) {
            return@withContext Resource.Error(e.toString())
        }
    }
}
